#include <stdio.h>
#include <stdlib.h>
#include "grafo.h"

// Busca, abre e retorna o arquivo de leitura
FILE* get_file(char* arqv){
    FILE* f = fopen(arqv, "r");
    if (!f) exit(1);
    return f;
}

// Preenche a estrutura do grafo com a quantidade de nós retirada da primeira linha do arquivo
TG* insert_nodes(TG* grafo, int nodes){
    int node_id;
    for (node_id = 1; node_id <= nodes; node_id++) insere_no(grafo, node_id);
    return grafo;
}

// Preenche a matriz com valor default = 0
void make_matrix(int n, int matrix[n][n]){
	int i, j;
	for(i = 0; i < n; i++){
		for (j = 0; j < n; j++){
			matrix[i][j] = 0;
		}
	}
}

// Imprime a matriz de adjacencias
void print_matrix(int n, int matrix[n][n]){
	if (!matrix) return;
	int row, columns, i = 1;
	printf("      ");
	for(i; i <= n; i++) printf("[%d]", i);
	puts("");
	for (row=0; row<n; row++){
		printf("[%d] - ", row+1);
	    for(columns=0; columns<n; columns++)
	        {
	         printf("|%d|", matrix[row][columns]);
	        }
	    printf("\n");
	 }
}

// Define os valores da matriz de adjacencias como 1 se existir uma aresta entre os nós
void assemble_matrix_from_file(FILE* file, int n, int matrix[n][n]){
	if ((!file) || (!matrix)) return;
	int a, b;
    while (fscanf(file, "%d %d", &a, &b) != EOF){
    	//printf("%d %d\n", a-1, b-1);
        matrix[a-1][b-1] = 1;
    }
}

//Verifica a partir da matriz de adjacencia se o grafo é orientado ou não
int is_oriented(int n, int matrix[n][n]){
	int i, j;
	for(i = 0; i < n-1; i++){
		for (j = 0; j < n; j++){
			if (matrix[i][j] && matrix[j][i]) return 1;
		}
	}
	return 0;
}

// Monta o grafo a partir de uma matriz
void assemble_edges_from_matrix(TG* grafo, int n, int matrix[n][n], int orientado){
    if ((!grafo) || (!matrix)) return;
    int i, j;
    int custo;
    //printf("Orientado: %d", orientado);
	for(i = 0; i < n; i++){
		custo = 0;
		for (j = 0; j < n; j++){
			if (matrix[i][j]){
				//printf("AOEFM -> %d %d\n", i+1, j+1);
				++custo;
				if(orientado) insere_aresta_orientado(grafo, i+1, j+1, custo);
				else insere_aresta_nao_orientado(grafo, i+1, j+1, custo);
			}
		}
	}
}

void get_infos(TG* grafo, int orientado){
	imprime(grafo);
	
	// Requisitos do trabalho
	if (orientado){
		puts("Grafo Orientado - Digrafo\n");
		conexao_forte(grafo);

	}else{
		puts("\t**Grafo nao Orientado**\n");
		if(grafo_conexo(grafo)){
			puts("* O grafo e conexo");
			puts("* Pontes do grafo:");
			//pontes(grafo);
			pontes_list(grafo);
			ponto_de_art(grafo);
		}else{
			puts("* O grafo nao e conexo");
			puts("* Componentes conexas");
			componentes_conexas(grafo);
		}
	}
}

//Comentado abaixo
void handle_answer(TG* grafo, int answer, int* orientado, int* n, int matrix[*n][*n]);


int checa_orientacao(TG* grafo){
	TNo* nodo =grafo->prim;
	while(nodo){
		TViz* v = nodo->prim_viz;
		while(v){
			if(busca_aresta(grafo, v->id_viz, nodo->id_no) == NULL) return 1;
			v = v->prox_viz;
		}
		nodo = nodo->prox_no;
	}
	return 0;
}

int main(int argc, char* argv[])
{
    puts("**GRAFOS** \n\n\tG12 (LUCAS RAMALHO LUIZ DA SILVA e YAGO DE REZENDE DOS SANTOS)\n\n");

    int answer = 1;
    char *file_name = argv[1]; //Parametro passado pelo terminal, por exemplo $./main example.txt --> {argv[1] = example.txt}
    //scanf("%s", file_name);

    FILE* file = get_file(file_name);

    int n;// Numero de nós
    fscanf(file, "%d", &n);

    printf("%d no(s) para processar\n", n);

    TG* grafo = cria();
    grafo = insert_nodes(grafo, n);

    int matrix[n][n];// Matriz de adjacencias utilizada para descobrir se o grafo é orientado ou não. matrix[no_de_saida][no_de_chegada]
    make_matrix(n, matrix);
	assemble_matrix_from_file(file, n, matrix);
	fclose(file);// O arquivo não é mais lido daqui para baixo

	int orientado = is_oriented(n, matrix);
	assemble_edges_from_matrix(grafo, n, matrix, orientado);

	get_infos(grafo, orientado);

	// Menu de operações apresentado para o usuário
    while(answer){
        puts("**OPERACOES COM O GRAFO**");
        puts("1 - Inserir um nodo");
        puts("2 - Remover nodo");
        puts("3 - Inserir uma aresta");
        puts("4 - Remover aresta");
        puts("5 - Imprimir o grafo");
        //puts("6 - Imprimir matriz de adjacencias");
        puts("6 - Imprimir propriedades do grafo");
        puts("0 - Encerrar programa");
        scanf("%d", &answer);
        puts("");

        handle_answer(grafo, answer, &orientado, &n, matrix);
    }

    libera(grafo);
    return 0;
}

// Executa as operações solicitadas pelo usuário
void handle_answer(TG* grafo, int answer, int* orientado, int* n, int matrix[*n][*n]){
    switch (answer){
        case 1:{
            *n += 1;
            insere_no(grafo, *n);
            printf("Nodo %d inserido!\n\n", *n);
            break;
        }
        case 2:{
            int no;
            printf("digite o id do nodo a ser removido: ");
            scanf("%d", &no);
            if (busca_no(grafo, no) != NULL){
                retira_no(grafo, no);
                printf("nodo %d removido!\n\n", no);
            }
            else puts("Este nodo nao existe neste grafo!");
            break;
        }
        case 3:{
            int a, b;
            printf("Digite os ids dos nodos a serem ligados: (a, b) = ");
            scanf("%d %d", &a, &b);
            if(busca_no(grafo, a) == NULL){
                printf("o nodo %d nao existe\n\n", a);
                break;
            }else if(busca_no(grafo, b) == NULL){
                printf("o nodo %d nao existe\n\n", b);
                break;
            }else{
                if (busca_aresta(grafo, a, b) != NULL){
                    puts("Esta Aresta ja existe!");
                    break;
                }else{
                    if (*orientado) insere_aresta_orientado(grafo, a, b, 1);
                    else insere_aresta_nao_orientado(grafo, a, b, 1);
                    *orientado = checa_orientacao(grafo);
                    puts("Aresta inserida!");
                }
            }
            break;
        }
        case 4:{
            int a, b;
            printf("Digite os ids dos nodos a serem removidos: (a, b) = ");
            scanf("%d %d", &a, &b);
            if(busca_no(grafo, a) == NULL){
                printf("o nodo %d nao existe\n\n", a);
                break;
            }else if(busca_no(grafo, b) == NULL){
                printf("o nodo %d nao existe\n\n", b);
                break;
            }else{
                if (busca_aresta(grafo, a, b) == NULL){
                    puts("Esta Aresta nao existe!");
                    break;
                }else{
                    if (*orientado) retira_aresta_orientado(grafo, a, b);
                    else retira_aresta_nao_orientado(grafo, a, b);
                    *orientado = checa_orientacao(grafo);
                    puts("Aresta inserida!");
                }
            }
            break;
        }
        case 5:
            puts("\t**GRAFO**");
            imprime(grafo);
            puts("\t**--**");
            break;
        case 6:
        	get_infos(grafo, *orientado);
        	break;
        case 77:
        	puts("      **MATRIZ DE ADJACENCIAS**");
            print_matrix(*n, matrix);
            puts("\t**--**");
            break;
        default:
            break;
    }
}