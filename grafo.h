#include "tlse.h"

typedef struct viz{
	int id_viz;
	int custo;
	struct viz *prox_viz;
}TViz;

typedef struct no{
	int id_no;
	TViz *prim_viz;
	struct no *prox_no;
}TNo;

typedef struct grafo{
	TNo *prim;
}TG;

TG *cria(void){
	TG *g = (TG*)malloc(sizeof(TG));
	g->prim = NULL;
	return g;
}

TNo *busca_no(TG *g, int no){
	TNo *p = g->prim;
	while(p && p->id_no != no){
		p = p->prox_no;
	}
	return p;
}

void insere_no(TG *g, int no){
	TNo *p  = busca_no(g,no);
	if(p) return;
	p = (TNo *)malloc(sizeof(TNo));
	p->id_no = no;
	p->prox_no = g->prim;
	p->prim_viz = NULL;
	g->prim = p;
}

TViz *busca_aresta(TG* g, int no1, int no2){
	TNo *viz1 = busca_no(g,no1);
	if(!viz1) return NULL;
	TNo *viz2 = busca_no(g,no2);
	if(!viz2) return NULL;
	TViz *p = viz1->prim_viz;
	while(p && p->id_viz!=no2){
		p = p->prox_viz;
	}
	return p;
}

void insere_aresta_orientado(TG *g,int no1, int no2, int custo){
	if(busca_aresta(g,no1,no2)) return;
	TNo *viz1 = busca_no(g,no1);
	if(!viz1) return;
	TNo *viz2 = busca_no(g,no2);
	if(!viz2) return;
	TViz *novo = (TViz *)malloc(sizeof(TViz));
	novo->custo = custo;
	novo->id_viz = no2;
	novo->prox_viz = viz1->prim_viz;
	viz1->prim_viz = novo;
}
void insere_aresta_nao_orientado(TG *g,int no1, int no2, int custo){
	if(busca_aresta(g,no1,no2)) return;
	TNo *viz1 = busca_no(g,no1);
	if(!viz1) return;
	TNo *viz2 = busca_no(g,no2);
	if(!viz2) return;
	TViz *novo = (TViz *)malloc(sizeof(TViz));
	novo->custo = custo;
	novo->id_viz = no2;
	novo->prox_viz = viz1->prim_viz;
	viz1->prim_viz = novo;
	TViz *novo2 = (TViz *)malloc(sizeof(TViz));
	novo2->custo = custo;
	novo2->id_viz = no1;
	novo2->prox_viz = viz2->prim_viz;
	viz2->prim_viz = novo2;
}

void retira_aresta_orientado(TG *g,int no1, int no2){
	TViz *b = busca_aresta(g,no1,no2);
	if(!b) return;
	TNo *a = busca_no(g,no1);
	if(a->prim_viz == b){
		a->prim_viz = b->prox_viz;
	}else{
		TViz *c = a->prim_viz;
		while(c->prox_viz != b){
			c = c->prox_viz;
		}
		c->prox_viz = b->prox_viz;
	}
	free(b);
}

void retira_aresta_nao_orientado(TG *g,int no1, int no2){
	retira_aresta_orientado(g,no1,no2);
	retira_aresta_orientado(g,no2,no1);
}

void retira_no(TG *g,int id){
	TNo *p = g->prim,*ant = NULL;
	TViz *a;
	while(p){
		a = busca_aresta(g,p->id_no,id);
		if(a) retira_aresta_orientado(g,p->id_no,id);
		p = p->prox_no;
	}
	p = g->prim;

	while((p) && (p->id_no != id)){
		ant = p;
		p = p->prox_no;
	}
	if(!p) return;
	TViz *v = p->prim_viz;
	while(v){
		retira_aresta_orientado(g,id,v->id_viz);
		v = p->prim_viz;
	}
	if(!ant){
		g->prim = p->prox_no;
	}else{
		ant->prox_no = p->prox_no;
	}
	free(p);
}

void imprime(TG *g){
	TNo *n = g->prim;
	TViz *v;
	while(n){
		printf("%d",n->id_no);
		v = n->prim_viz;
		while(v){
			printf(" --> %d (%d)",v->id_viz,v->custo);
			v = v->prox_viz;
		}
		printf("\n");
		n = n->prox_no;
	}
}

void libera(TG* g){
    TNo *p = g->prim;
    while(p){
        TViz* v = p->prim_viz;
        while(v){
            TViz* t = v;
            v = v->prox_viz;
            free(t);
        }
        TNo* q = p;
        p = p->prox_no;
        free(q);
    }
}
void copia_nos(TG *g,TNo *n){
	if(n){
		copia_nos(g,n->prox_no);
		insere_no(g,n->id_no);
	}
}
TG *copia_grafo(TG *g){
	TNo *n = g->prim;
	TViz *v;
	TG *resp = cria();
	copia_nos(resp,n);
	
	n = g->prim;

	while(n){
		v = n->prim_viz;
		while(v){
			insere_aresta_orientado(resp,n->id_no,v->id_viz,v->custo);
			v = v->prox_viz;
		}
		n = n->prox_no;
	}
	return resp;
}

TLSE *conexao_forte_op(TG *g,TNo *no_atual,int no_ini,TViz *v,TLSE *l,TLSE *resp){
	int i,c,t;TNo *p;
	v = no_atual->prim_viz;
	while(v){
		if(v->id_viz != no_ini){
			if(!busca(l,v->id_viz)){
				l = ins_fim(l,v->id_viz);
				t=1;
			}else{
				t=0;
			}
			
			i = v->id_viz;
			c = v->custo;
			v = v->prox_viz;
			p = busca_no(g,i);
			retira_aresta_orientado(g,no_atual->id_no,i);
			resp = conexao_forte_op(g,p,no_ini,p->prim_viz,l,resp);
			insere_aresta_orientado(g,no_atual->id_no,i,c);
			if(t){
				retira_ultimo(l);
			}
			
		}else{
			v = v->prox_viz;
		}
		
		
	}
	if(busca_aresta(g,no_atual->id_no,no_ini)){
		if(tamanho(l)>tamanho(resp)){
			return copia(l);
		}
	}
	return resp;

}

void conexao_forte(TG *c){
	TG *g = copia_grafo(c);
	
	TViz *v;
	TLSE *l;
	TLSE *resp;
	int i=1;
	while(g->prim){
		l = inicializa();
		l = ins_fim(l,g->prim->id_no);
		resp = inicializa();
		resp = conexao_forte_op(g,g->prim,g->prim->id_no,v,l,resp);
		free(l);
		if(resp){
				printf("%d componente fortemente conexa: ",i);imprime_lista(resp);printf("\n");
				while(resp){
					retira_no(g,resp->info);
					resp = resp->prox;
			}
			libera_lista(resp);
			i++;
		}else{
			retira_no(g,g->prim->id_no);
		}
	
		
		
		
		
	}
	if(i == 1){
		printf("Não existem componentes fortemente conexas neste grafo!\n");
	}
	libera(g);
}


int grafo_conexo(TG *g){
	TLSE *l = inicializa();
	TG *c = copia_grafo(g);
	l = ins_fim(l,c->prim->id_no);
	int i = grafo_conexo_op(c,c->prim,l);
	libera(c);
	free(l);
	if(i == 1){
		//printf("O grafo e conexo!\n");
		return 1;
	}else{
	//	printf("O grafo nao e conexo!\n");
		return 0;
	}

}

int grafo_conexo_op(TG *g,TNo *n,TLSE *l){
	TViz *v = n->prim_viz;int i;
	while(v){
		if(!busca(l,v->id_viz) && busca_no(g,v->id_viz)){
			l = ins_fim(l,v->id_viz);
			i = v->id_viz;
			v = v->prox_viz;
			grafo_conexo_op(g,busca_no(g,i),l);
			retira_ultimo(l);
		}else{
			v = v->prox_viz;
		}

	}
	retira_no(g,n->id_no);
	if(g->prim) return 0;
	return 1;
}

void ponto_de_art_op(TG *g,TNo *n){
	if(!n) return;
	TG *c = copia_grafo(g);
	retira_no(c,n->id_no);
	//imprime(c);
	if(!grafo_conexo(c)) printf("%d\n",n->id_no);
	libera(c);
	ponto_de_art_op(g,n->prox_no);

}

int pont_list_exist(TLSE* inicio, TLSE* fim, int id1, int id2){
    if (!inicio || !fim) return 0;
    while(inicio != NULL){
        if(inicio->info == id1 && fim->info == id2) return 1;
        if(inicio->info == id2 && fim->info == id1) return 1;
        inicio = inicio->prox;
        fim = fim->prox;
    }
    return 0;
}

void pontes_list(TG* grafo){
    if (!grafo) return;
    TG* g = copia_grafo(grafo);
    TNo* nodo = g->prim;
    TViz* v;
    TLSE* inicio = inicializa(), *fim = inicializa();
    while(nodo){
        v = nodo->prim_viz;
        while(v){
            int nodo_id = nodo->id_no, viz_id = v->id_viz;
            if(!pont_list_exist(inicio, fim, nodo_id, viz_id)){
                int custo = v->custo;
                v = v->prox_viz;
                retira_aresta_nao_orientado(g, nodo_id, viz_id);
                if(!grafo_conexo(g)){
                    printf("  *%d %d\n", nodo_id, viz_id);
                    inicio = ins_fim(inicio, nodo_id);
                    fim = ins_fim(fim, viz_id);
                }
                insere_aresta_nao_orientado(g, nodo_id, viz_id, custo);
            }else v = v->prox_viz;
        }
        nodo = nodo->prox_no;
    }
    libera(g);
    free(nodo);
    free(v);
    libera_lista(inicio);
    libera_lista(fim);
}

void ponto_de_art(TG *g){
	printf("Os pontos de articulacao do grafo sao:\n");
	ponto_de_art_op(g,g->prim);
}

TLSE* pega_comp(TG* grafo, TLSE* l, int nodo_id){
    if(!grafo || !l) return l;
    TNo* nodo = busca_no(grafo, nodo_id);
    TViz* v = nodo->prim_viz;
    //printf(" %d", v->id_viz);
    while(v){
        //Verificar se o no já foi visitado
        if(busca(l, v->id_viz) == NULL){
            l = pega_comp(grafo, ins_fim(l, v->id_viz), v->id_viz);
        }
        v = v->prox_viz;
    }
    return l;
}

void componentes_conexas(TG* grafo){
    TNo* nodo = grafo->prim;
    TLSE* encontrados = inicializa();
    while(nodo){
        TLSE* l = inicializa();
        l = ins_ini(l, nodo->id_no);
        //imprime_lista(l);
        l = pega_comp(grafo, l, nodo->id_no);
        imprime_lista(l);
        puts("");
        //Procura proxima raiz do componente
        encontrados = join_list(encontrados, l);
        while(nodo && busca(encontrados, nodo->id_no) != NULL) nodo = nodo->prox_no;
        libera_lista(l);
    }
    libera_lista(encontrados);
}