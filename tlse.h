typedef struct lista{
	int info;
	struct lista *prox;
}TLSE;

TLSE *inicializa(void){
	return 	NULL;
}

TLSE *ins_ini(TLSE *l, int elem){
	TLSE *novo = (TLSE *)malloc(sizeof(TLSE));
	novo->info = elem;
	novo->prox = l;
	return novo;
}

TLSE *ins_fim(TLSE *l, int elem){
	if(!l){
		return ins_ini(l,elem);
	}
	TLSE *p = l;
	while(p->prox){
		p = p->prox;
	}
	p->prox = ins_ini(NULL,elem);
	return l;
}

TLSE *ins_fim_rec(TLSE *l,int elem){
	if(!l){
		return ins_ini(l,elem);
	}
	l->prox = ins_fim_rec(l->prox,elem);
	return l;
}

TLSE *busca(TLSE *l, int elem){
	while(l && l->info != elem){
		l=l->prox;
	}
	return l;
}

TLSE *busca_rec(TLSE *l,int elem){
	if(!l){
		return NULL;
	}
	if(l->info == elem){
		return l;
	}
	return busca_rec(l->prox,elem);
}

void imprime_lista(TLSE *l){
	while(l){
		printf("%d ",l->info);
		l= l->prox;
	}
}

void imprime_rec(TLSE *l){
	if(l){
	printf("%d",l->info);
	imprime_rec(l->prox);
    }
}

void retira_ultimo(TLSE *l){
	if(!l) return;
	TLSE *ant;
	while(l->prox){
		ant = l;
		l = l->prox;
	}
	ant->prox = NULL;
	free(l);
}
void libera_lista(TLSE *l){
	while(l){
		TLSE *p=l;
		l=l->prox;
		free(p);
	}
}

void libera_rec(TLSE *l){
	if(l){
		libera_rec(l->prox);
		free(l);
	}
}

int tamanho(TLSE *l){
	int resp = 0;
	while(l){
		resp++;
		l = l->prox;
	}
	return resp;
}

TLSE *copia(TLSE *l){

	TLSE *resp = inicializa();
	while(l){
		resp = ins_fim(resp,l->info);
		l = l->prox;
	}
	return resp;
}

// Junta duas listas encadeadas
TLSE* join_list(TLSE* list1, TLSE* list2){
    if (!list2) return list1;
    while(list2){
        list1 = ins_fim(list1, list2->info);
        list2 = list2->prox;
    }
    return list1;
}